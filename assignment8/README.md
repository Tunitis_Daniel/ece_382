#Assignment 8 - Moving Average
#### Daniel Tunitis

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/8c1693cd4f347f46c292f38d5f6f866dc862bab8/assignment8/n_avg21.png?token=28cadba846205c49b1f152f559b3b19de32e1289)

#####Figure 1  proof of sample 1 with size of 2

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/8c1693cd4f347f46c292f38d5f6f866dc862bab8/assignment8/n_avg41.png?token=4a3e49b42a51fd5fbfd5ea6ae7b6bb477635a409)

#####Figure 2 proof of sample 1 with size of 4

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/8c1693cd4f347f46c292f38d5f6f866dc862bab8/assignment8/n_avg22.png?token=b8cbce290caf92180a3f1ab9b02ea02e29edc5be)

#####Figure 3 proof of sample 2 with size of 2

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/b789de5f44f8112e61d1be093efdf81745eecf88/assignment8/n_avg42.png?token=069d6f02db8a9af6540614b905da0621a3485778)

##### Figure 4 proof of sample 2 with size of 4

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/b789de5f44f8112e61d1be093efdf81745eecf88/assignment8/maxmin_1.png?token=990a5dba2560859794d65143ed86f39da9e5bcf8)

##### Figure 5 proof of working range finder

For the arrays I chose integers, as they are numbers, and the array is thus then a pointer to a bunch of integers.

To test the data average function I used the given data streams and sample sizes as asked. I also tested the second set of data with my range function to verify that my range function works, which in turn verifies the max and min functions also work. 

####Documentation: None 
