/*--------------------------------------------------------------------
Name: C2C Daniel Tunitis
Date: 11 October 2016
Course: ECE 382
File: moving_average.c
Event: Assignment 8 - Moving Average

Purp: updates and monitors a moving average

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#define N_AVG_SAMPLES 2
void getAverage(int array[], unsigned int arrayLength, int result[]){

	int samples[N_AVG_SAMPLES];
	int k = 0;
	while(k < N_AVG_SAMPLES){
		samples[k] = 0;
		k++;
	}
	int i = 0;
	while(i <= arrayLength){
		int c = 0;
		int avg = 0;
		while(c < N_AVG_SAMPLES){
			avg += samples[c];
			c++;
		}

		result[i] = avg/N_AVG_SAMPLES;
		c = 0;
		while(c < N_AVG_SAMPLES-1){
			samples[c] = samples[c+1];
			c++;
		}
		samples[N_AVG_SAMPLES-1] = array[i];
		i++;
	}

}

int max(int array[], unsigned int arrayLength){
	int i = 0;
	int max = array[i];
	while (i < arrayLength){
		if(array[i] > max){
			max = array[i];
		}
		i++;
	}
	return max;
}
int min(int array[], unsigned int arrayLength){
	int i = 0;
	int min = array[i];
	while (i < arrayLength){
		if(array[i] < min){
			min = array[i];
		}
		i++;
	}
	return min;
}

void range(int array[], unsigned int arrayLength, int* amax, int* amin){

	*amax = max(array, arrayLength);
	*amin = min(array, arrayLength);
}
