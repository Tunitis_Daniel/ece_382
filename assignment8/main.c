#include <msp430.h> 
#include "moving_average.h"
/*--------------------------------------------------------------------
Name: C2C Daniel Tunitis
Date: 11 October 2016
Course: ECE 382
File: main.c
Event: Assignment 8 - Moving Average

Purp: checks moving average functions

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    int numbers[10];
    numbers[0] = 174;
    numbers[1] = 162;
    numbers[2] = 149;
    numbers[3] = 85;
    numbers[4] = 130;
    numbers[5] = 149;
    numbers[6] = 153;
    numbers[7] = 164;
    numbers[8] = 169;
    numbers[9] = 173;
    unsigned int len = 11;
    //int result[11];
    //getAverage(numbers, len, result);

    int mymax = 0;
    int mymin = 0;
    range(numbers, len-1, &mymax, &mymin );


	return 0;
}
