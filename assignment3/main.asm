;-------------------------------------------------------------------------------
; Daniel Tunitis
;ECE 382
; Assignment 3
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
			;mov.w 	&0x0216, r6 			; read from 0x0216
			cmp.w   #0x1234, &0x0216
			jz 		less
			jl		less
			mov.w  #0x0014, r6
			and.w  r3, &0x0206
loop1: ;loop to add 20 + 19 + ... 1
			add.w 	r6, &0x0206
			dec		r6
			cmp.w   r6, r3
			jne     loop1
			jmp 	forever

less: ;loop for the value being less than 0x1234
			cmp.w #0x1000, &0x0216
			jl    option2
			jz	  option2 ; jump if less than 0x1000


			;add the carry flag to memory
			and.b r3, &0x0202
			add.w #0xEEC0, &0x0216
			adc &0x0202
			jmp forever

option2: ; its less than 0x1000
			mov.w &0x0216, &0x0212
			;and.w r3, r7 replace with a move
			mov.w #0x0001, r7
			and.w &0x0212, r7
			jz    even
			rla.w &0x0212
			jmp forever

even:
			rra.w &0x0212
			jmp forever
                                            
forever:	jmp forever
;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
