;-------------------------------------------------------------------------------
; Lab1- Assembly Calculator
; C2C Daniel Tunitis, USAF
;
; Purpose: This program uses assembly to create a calculator that uses ROM for instructions
; and numbers storing the result in RAM
;
; Documentation: EI with Captain Falkinburg
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file
            
;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory.
myProgram:	.byte		0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55

ADD_OP:		.equ		0x11
SUB_OP:		.equ		0x22
CLR_OP:		.equ		0x44
END_OP:		.equ		0x55
MUL_OP:		.equ		0x33
MAX:		.equ		0xff
TWO:		.equ		0x02
ONE:		.equ		0x01
RAM_START:  .equ		0x200
            .retain                         ; Override ELF conditional linking
                                            ; and retain current section.
            .retainrefs                     ; And retain any sections that have
                                            ; references to current section.

;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer


;-------------------------------------------------------------------------------
; Main loop here
;-------------------------------------------------------------------------------
Main:
		;Read program from ROM
		mov		#RAM_START, r6
		mov 	#myProgram, r5
		mov.b		@r5+, r8		;read first number from ROM
Read_Op:
		mov.b		@r5+, r9		; read operation from ROM
		cmp			#ADD_OP, r9
		jeq			ADD
		cmp			#SUB_OP, r9
		jeq			SUB
		cmp			#CLR_OP, r9
		jeq			CLR
		cmp			#MUL_OP, r9
		jeq 		MUL
		cmp			#END_OP, r9
		jeq			Forever
		;if not add, subb, clr, it will act as a END
		jmp 		Forever

ADD:
		mov.b		@r5+, r10
		add			r10, r8
		jmp			CHECK
SUB:
		mov.b		@r5+, r10
		sub			r10, r8
		jmp 		CHECK
CLR:
		mov.b		#0x00, 0(r6)
		inc			r6
		mov.b 		@r5+, r8
		jmp			Read_Op
MUL:

; check last bit, if 1, its odd, add 1 to r8
; now shift them until the divisor is 1, shift the other opposite direction
		mov.b		@r5+, r10
		cmp			r3, r10
		jeq			ZERO
		mov.b 		r10, r7
		and			#ONE, r7
		;If one, it will have one in it, if not, it will be zero

loop:
		cmp			#TWO, r10
		jl			END_MUL
		rla			r8			;multiply it by 2
		rra			r10
		jmp 		loop

END_MUL:
		add			r8, r7		;accounts for odd numbers
		mov			r7, r8
		jmp			CHECK
ZERO:
		mov			r3, r8
		jmp 		STORE

CHECK:
		cmp			#MAX, r8
		jge			GREATER
		cmp			r3, r8
		jl			LESS
		jmp			STORE

GREATER:  ;Greater than 0xff, set the register to 255
		mov.b		#MAX, r8
		jmp STORE
LESS:		;Less than 0, set register to 0
		mov.b		r3, r8
		jmp STORE
STORE:
		mov.b		r8, 0(r6)
		inc			r6
		jmp 		Read_Op

Forever: jmp Forever

;-------------------------------------------------------------------------------
; Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect   .stack
            
;-------------------------------------------------------------------------------
; Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
            
