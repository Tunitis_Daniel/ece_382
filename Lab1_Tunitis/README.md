# Lab1- Assembly Language - "A Simple Calculator"

## By Daniel Tunitis

### Purpose
To develop a calculator program that reads bytes of instructions from ROM and outputs results to memory starting at 0x200 in RAM

### Preliminary Design

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/c282222d62d73847671c43c01474bf10ed17fe73/Lab1_Tunitis/prelab.png?token=56616de9fa4a9f38c8ea2802a4a43125e78ed636)

Figure 1. Flowchart showing basic design of program

For B functionality I decided the best course of action would be to develop a check method to determine if the value was within the valid range

For A functionality log(n) time was needed for the multiply function. To develop this I figured that shifts would have to be used, and planned to use them to simulate multiplications of 2, to avoid having a function that worked at O(n) time.

#### Code:

See main.asm for complete code

###### Check Function

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/81d44f1abcfadb800c76db91fef71036c27235f6/Lab1_Tunitis/checker.png?token=fb17857df828f8e5a1a7a9e9eece9949364a081e)

###### Multiply function

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/ce703b3d0c8ba390676ff4812451f3999b887448/Lab1_Tunitis/mul_op.png?token=034665c36bdb9a8c2852ff56530d978d84a3ab2f)

This code works by checking to see if it is multiplying by zero, and if so, stores 0. It then checks through an and if the number is odd. If it is odd, it will have 1 in r7, and r7 will be added to the product of the shifts at the completion of the multiplication. The function works in O(log(n)) time as it shifts for multiples of 2, making it perform multiplication in logarithmic time.
 
###### Assembly header

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/81d44f1abcfadb800c76db91fef71036c27235f6/Lab1_Tunitis/header.png?token=201f8dce2c3e07572c3bb2e4a7e81f3283eeb64e)

### Debugging
The main issue I ran into was incorrect products from my multiplication method. I was getting incorrect values that were much larger than anticipated. I realized that this was due to me decrementing the multiplier, rather than shifting it to the right. I also had to come up for a solution to multiplying odd numbers, which I solved by performing an AND on the multiplier to check for a 1 in the LSB, and adding the result of this to the multiplication at the end. 

### Testing Methodology and Results

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/ce703b3d0c8ba390676ff4812451f3999b887448/Lab1_Tunitis/memory_mapA.png?token=d78d4514d0a72232f4a071fad058ace5580da4af)

Figure 3 A Functionality Result

Firs the A functionality code was tested: 0x22, 0x11, 0x22, 0x22, 0x33, 0x33, 0x08, 0x44, 0x08, 0x22, 0x09, 0x44, 0xff, 0x11, 0xff, 0x44, 0xcc, 0x33, 0x02, 0x33, 0x00, 0x44, 0x33, 0x33, 0x08, 0x55

This tests addition of two positives, subtraction to a positive result, multiplication to a positive result, subtraction to a negative value (results in 0x00), CLR op, and results that exceed the positive max and zero minimum. Additionally, it tests multiplying by zero.

Additional edge cases are tested

0x00, 0x00, 0x00

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/aed932b453169f16dbbeeefef8fdc8820c9db9b1/Lab1_Tunitis/test1.png?token=ce0db1fa0faaf004c27572bd02b7d923f087956e)

This tests an invalid operator. When the invalid operator is reached, it is treated as an end program. We can see this works as our memory remains unchanged, no operations have occurred.

0x22, 0x11, 0x22, 0x55, 0x22, 0x22

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/aed932b453169f16dbbeeefef8fdc8820c9db9b1/Lab1_Tunitis/test2.png?token=84777378d62e651ec5878e508af6b8284ac4b430)

This tests to make sure that the end program command works and commands after this are not run. We can see that it works as nothing is stored in memory after the 0x55 command is run.

Through these cases we have tested for any bugs that could arise. Having tested normal addition and subtraction, along with bad operations, multiplying by zero, and operations which result in a value out of range, we can be confident the calculator will work as needed.

### Lab Questions

N/A

### Observations and Conclusions

The purpose of this lab was to develop an assembly calculator which reads instructions from ROM and outputs the results in RAM. The objective was accomplished. In this I learned how to trouble shoot with assembly. It also helped me in the planning process. I also learned how to use Visio efficiently after I lost .5 points on the prelab for my diagram. In future labs I will now be able to make flowcharts with lightning speed. 

### Documentation:
EI from Captain Falkinburg on the program and lab report