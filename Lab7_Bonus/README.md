#Lab 7 Bonus Functionality

This is a library of standalone functions for the servo and ultrasonic sensor. It has the code to set the sensor and servo up, as well as code for checking in each direction, which also returns an int which could be interfaced with in later labs to determine how far you are from the walls.

I verified it worked by testing it with the main.c file on my robot. I also improved the speed from the required functionality, so that it accomplishes readings faster.

###Documentation: 
EI with Capt Falkinburg for Lab7 C2C Johnson pointed out that you can set upTa0 on P1 pins