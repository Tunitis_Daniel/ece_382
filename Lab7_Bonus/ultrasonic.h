/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <28 November 2016>
Course: <ECE 382>
File: <ultrasonic.h>
Event: <Lab 7>

Purp: Header file for standalone servo library

Doc:    The button code is from Lab 5, setup is from lab 6. C2C Johnson pointed out that you can set up
Ta0 on P1 pins

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#ifndef ULTRASONIC_H_
#define ULTRASONIC_H_

//#include "movement.h"
#define   	ECHOPIN (P1IN & BIT4)

void setup(); // Sets up the pins for the ultrasonic sensor and servo
int getdistright(); //turns the sensor right and takes a reading
int getdistleft(); //turns the sensor left and takes a reading
int getdistcenter(); //turns the sensor to the front and takes a reading
void setup(); //sets up pins for the ultrasonic sensor



#endif /* ULTRASONIC_H_ */
