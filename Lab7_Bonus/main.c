/*
 * main.c
 *
 *  Created on: Dec 2, 2016
 *      Author: C18Daniel.Tunitis
 */
#include <msp430.h>
#include "ultrasonic.h"
void main(){
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;  //8 MHz clock
	setup();
	int this = 2;
	while(1){
		this = getdistright();
		__delay_cycles(20000000);
		this = getdistleft();
		__delay_cycles(20000000);
		this = getdistcenter();
		__delay_cycles(20000000);
	}
}


