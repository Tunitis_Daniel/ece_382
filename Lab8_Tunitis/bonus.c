#include <msp430.h>
#include "ultrasonic.h"
#include "movement.h"
/*
 * main.c
 */
int main(void) {
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;  //8 MHz clock
	P1DIR |= BIT0; //set up LED1
	P1DIR |= BIT6; //set up LED2
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	P2DIR |= BIT1;                // TA1CCR1 on P2.1
	P2SEL |= BIT1;                // TA1CCR1 on P2.1
	P2OUT &= ~BIT1;
	P2DIR |= BIT4;				//TA1.2 on P2.4
	P2SEL |= BIT4;
	P2OUT &= ~BIT4; //clear output bit

	P1DIR |= BIT2;
	P1SEL |= BIT2;
	P1OUT &= ~BIT2; //set up P1.2 w/ TA0CRR1 for servo

	TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
  //  P1DIR = BIT0;            //use LED to indicate duty cycle has toggled
  //  P1REN = BIT3;
// P1OUT = BIT3;

	TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 500;                // set duty cycle to 250/1000 (25%)
	TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
	TA1CCR2 = 500;
	TA1CCTL2 |= OUTMOD_3;

	TA0CTL |= TASSEL_2|MC_1|ID_3; //configure for SMCLK
	TA0CCR0 = 20000; //set signal period to ~1.5 ms
	TA0CCR1 = 1500;
	TA0CCTL1 |= OUTMOD_7;

	TA0R = 0; //clear TAR

	P2DIR |= BIT5;   //set up p2.5 as GPIO
	P2SEL &= ~BIT5;
	P2SEL2 &= ~BIT5;
	P2DIR |= BIT3;                      // Set up P2.3 as GPIO not XIN
	P2SEL &= ~BIT3;                                                      // This action takes
	P2SEL2 &= ~BIT3;
	P1DIR |= BIT5;
	P1SEL &= ~BIT5;
	P1SEL2 &= ~BIT5;
	P1OUT &= ~BIT5;


	P1DIR &= ~BIT4; //set up P1.3 as GPIO for echo output from sensor
	P1SEL &= ~BIT4;
	P1SEL2 &= ~BIT4;
	P1DIR |= BIT1; //set up P1.3 as GPIO for input to trig
	P1SEL &= ~BIT1;
	P1SEL2 &= ~BIT1;
	P1OUT &= ~BIT1; //start it default to off
	P1OUT |= BIT2;
	P1OUT &= ~BIT5; //freeze the wheels


	volatile int resultleft = 0;
	volatile int resultcenter = 0;
	volatile int resultright = 0;
	volatile int correct = 0;
	getdistleft();
	__delay_cycles(2000000);
	//get to the end of the first turn
	resultleft = getdistleft();
	getdistright();
	__delay_cycles(2000000);
		//get to the end of the first turn
	resultright = getdistright();
	__delay_cycles(2000000);
	getdistcenter();
	__delay_cycles(2000000);
	resultcenter = getdistcenter();
	while(1){
		while(resultcenter > 10*148){
			correctleft();
			__delay_cycles(2000000);
			correct = correctleft();
			if(correct < 6*148){
				__delay_cycles(2000000);
				slight_right();
				__delay_cycles(2000000);
			}
			correctright();
			__delay_cycles(2000000);
			correct = correctright();
			if(correct < 6* 148){
				__delay_cycles(2000000);
				slight_left();
				__delay_cycles(2000000);
			}
			half_forward();
			getdistcenter();
			__delay_cycles(2000000);
			resultcenter = getdistcenter();
		}
		getdistleft();
		__delay_cycles(2000000);
		resultleft = getdistleft();
		if(resultleft > 15*148){
			tankleft();
		}
		else{
			tankright();
		}
		getdistcenter();
		__delay_cycles(2000000);
		resultcenter = getdistcenter();
	}

}
