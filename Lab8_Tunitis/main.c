#include <msp430.h> 
#include "ultrasonic.h"
#include "movement.h"
/*
 * main.c
 */
int main(void) {
	WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;  //8 MHz clock
	P1DIR |= BIT0; //set up LED1
	P1DIR |= BIT6; //set up LED2
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	P2DIR |= BIT1;                // TA1CCR1 on P2.1
	P2SEL |= BIT1;                // TA1CCR1 on P2.1
	P2OUT &= ~BIT1;
	P2DIR |= BIT4;				//TA1.2 on P2.4
	P2SEL |= BIT4;
	P2OUT &= ~BIT4; //clear output bit

	P1DIR |= BIT2;
	P1SEL |= BIT2;
	P1OUT &= ~BIT2; //set up P1.2 w/ TA0CRR1 for servo

	TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
  //  P1DIR = BIT0;            //use LED to indicate duty cycle has toggled
  //  P1REN = BIT3;
// P1OUT = BIT3;

	TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 500;                // set duty cycle to 250/1000 (25%)
	TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
	TA1CCR2 = 500;
	TA1CCTL2 |= OUTMOD_3;

	TA0CTL |= TASSEL_2|MC_1|ID_3; //configure for SMCLK
	TA0CCR0 = 20000; //set signal period to ~1.5 ms
	TA0CCR1 = 1500;
	TA0CCTL1 |= OUTMOD_7;

	TA0R = 0; //clear TAR

	P2DIR |= BIT5;   //set up p2.5 as GPIO
	P2SEL &= ~BIT5;
	P2SEL2 &= ~BIT5;
	P2DIR |= BIT3;                      // Set up P2.3 as GPIO not XIN
	P2SEL &= ~BIT3;                                                      // This action takes
	P2SEL2 &= ~BIT3;
	P1DIR |= BIT5;
	P1SEL &= ~BIT5;
	P1SEL2 &= ~BIT5;
	P1OUT &= ~BIT5;


	P1DIR &= ~BIT4; //set up P1.3 as GPIO for echo output from sensor
	P1SEL &= ~BIT4;
	P1SEL2 &= ~BIT4;
	P1DIR |= BIT1; //set up P1.3 as GPIO for input to trig
	P1SEL &= ~BIT1;
	P1SEL2 &= ~BIT1;
	P1OUT &= ~BIT1; //start it default to off
	P1OUT |= BIT2;
	P1OUT &= ~BIT5; //freeze the wheels
	__delay_cycles(2000000);
	volatile int result = 0;
	getdistleft();
	__delay_cycles(222);
	//get to the end of the first turn
	while(result < (26*148)){
		if(result > (6*148)){
			slight_left();
		}
		half_forward();
		__delay_cycles(5000000);
		result = getdistleft();
		__delay_cycles(2000);
		if(result < (2*148)){
			slight_right();
		}

	}
	__delay_cycles(2000000);
	result = getdistcenter();
	if(result > 9*148){
		half_forward();
	}
	move_left();
	__delay_cycles(5000000);
	getdistleft();
	__delay_cycles(5000000);
	result = getdistleft();
	__delay_cycles(200000);
	if(result > 7 *148){
		slight_left();
	}
	getdistright();
	__delay_cycles(5000000);
	result = getdistright();
	__delay_cycles(200000);
	if(result < 2 *148){
		slight_left();
	}
	__delay_cycles(2000);
	result = getdistright();
	while(result < (10*148)){
		half_forward();
		result = getdistright();
		if(result < (2*148)){
			slight_left();
		}
		__delay_cycles(200000);

	}
	move_right();
	__delay_cycles(200000);
	result = getdistleft();
	__delay_cycles(2000);
	if(result < 3 * 148){
		slight_right();
	}
	__delay_cycles(2000);

	half_forward();
	__delay_cycles(20000);
	result = getdistright();
	while(result < (10*148)){
		move_forward();
		__delay_cycles(20000);
		result = getdistright();
	}
	quarterforward();
	move_right();
	getdistcenter();
	__delay_cycles(2000000);
	result = getdistcenter();
	while(result > (15*148)){
		half_forward();
		__delay_cycles(200000);
		result = getdistright();
		if(result < (3*148)){
			slight_left();
		}
		getdistcenter();
		__delay_cycles(2000000);
		result = getdistcenter();
		__delay_cycles(200000);
	}
	move_left();
	getdistright();
	__delay_cycles(2000000);
	result = getdistright();
	if(result < (4*148)){
		slight_left();
	}
	getdistcenter();
	half_forward();
	__delay_cycles(200000);
	result = getdistcenter();
	while(result < (25*148)){
		slight_right();
		quarterforward();
		slight_left();
		__delay_cycles(200000);
		result = getdistcenter();
		__delay_cycles(200000);
	}
	move_forward();
}
