

#ifndef MOVEMENT_H_
#define MOVEMENT_H_

void move_forward(); //Moves forward for small period of time
void move_reverse(); //Reverses direction for small period of time
void move_left();    //Turns left with one wheel off
void move_right();   //Turns right with one wheel off
void slight_right();
void slight_left();
void half_forward();
void quarterforward();
void tankleft();
void tankright();

#endif /* MOVEMENT_H_ */
