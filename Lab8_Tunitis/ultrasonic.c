#include <msp430.h> 
#include "ultrasonic.h"
/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <28 November 2016>
Course: <ECE 382>
File: <ultrasonic.c>
Event: <Lab 7>

Purp: To be used as a standalone library for the Lab7 Bonus functionality

Doc:    The button code is from Lab 5, setup is from lab 6. C2C Johnson pointed out that you can set up
Ta0 on P1 pins

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/


volatile int time = 0;
void setup(){
	P1DIR |= BIT0; //set up LED1
	P1DIR |= BIT6; //set up LED2
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	P1DIR |= BIT2;
	P1SEL |= BIT2;
	P1OUT &= ~BIT2; //set up P1.2 w/ TA0CRR1 for servo
    TA0CTL |= TASSEL_2|MC_1|ID_3; //configure for SMCLK
    TA0CCR0 = 20000; //set signal period to ~1.5 ms
    TA0CCR1 = 1500;
    TA0CCTL1 |= OUTMOD_7;

    TA0R = 0; //clear TAR
    P1DIR &= ~BIT4; //set up P1.3 as GPIO for echo output from sensor
	P1SEL &= ~BIT4;
	P1SEL2 &= ~BIT4;
	P1DIR |= BIT1; //set up P1.3 as GPIO for input to trig
	P1SEL &= ~BIT1;
	P1SEL2 &= ~BIT1;
	P1OUT &= ~BIT1; //start it default to off
	P1OUT |= BIT2;
	P1OUT &= ~BIT5; //freeze the wheels

}
int getdistright(){
	TA0CCR1 = 600; //right turn
	__delay_cycles(200000);
	TA0CCR0 = 680000;
	P1SEL &= ~BIT2;
	P1OUT |= BIT1;
	__delay_cycles(80); //approx 1 us delay
	P1OUT &= ~BIT1;   //turn off the trigger
	//TAR = 0;
	while(ECHOPIN == 0);
	TA0R = 0;
	while(ECHOPIN != 0);
	time = TA0R;
	if(time < 800){
		P1OUT |= BIT6;
	}

	TA0CCR0 = 20000;
	TA0R = 0;
	P1SEL |= BIT2;
	__delay_cycles(200000);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	return time;
}

int getdistcenter(){
	TA0CCR1 = 1500;
	P1OUT |= BIT2;
	__delay_cycles(200000);
	TA0CCR0 = 680000;
	P1SEL &= ~BIT2;
	P1OUT |= BIT1;
	__delay_cycles(80); //approx 1 us delay
	P1OUT &= ~BIT1;   //turn off the trigger
	//TAR = 0;
	while(ECHOPIN == 0);
	TA0R = 0;
	while(ECHOPIN != 0);
	time = TA0R;
	if(time < 800){
		P1OUT |= BIT0;
		P1OUT |= BIT6;
	}

	TA0CCR0 = 20000;
	TA0R = 0;
	P1SEL |= BIT2;
	__delay_cycles(200000);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	return time;
}

int getdistleft(){
	TA0CCR1 = 2400; //left turn
	__delay_cycles(200000);
	TA0CCR0 = 680000;
	P1SEL &= ~BIT2;
	P1OUT |= BIT1;
	__delay_cycles(80); //approx 1 us delay
	P1OUT &= ~BIT1;   //turn off the trigger
	//TAR = 0;
	while(ECHOPIN == 0);
	TA0R = 0;
	while(ECHOPIN != 0);
	time = TA0R;
	if(time < 800){
		P1OUT |= BIT0;
	}

	TA0CCR0 = 20000;
	TA0R = 0;
	P1SEL |= BIT2;
	__delay_cycles(200000);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	return time;
}
int correctleft(){
	TA0CCR1 = 2200; //left turn
	__delay_cycles(200000);
	TA0CCR0 = 680000;
	P1SEL &= ~BIT2;
	P1OUT |= BIT1;
	__delay_cycles(80); //approx 1 us delay
	P1OUT &= ~BIT1;   //turn off the trigger
	//TAR = 0;
	while(ECHOPIN == 0);
	TA0R = 0;
	while(ECHOPIN != 0);
	time = TA0R;
	if(time < 800){
		P1OUT |= BIT0;
	}

	TA0CCR0 = 20000;
	TA0R = 0;
	P1SEL |= BIT2;
	__delay_cycles(200000);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	return time;
}
int correctright(){
	TA0CCR1 = 700; //right turn
	__delay_cycles(200000);
	TA0CCR0 = 680000;
	P1SEL &= ~BIT2;
	P1OUT |= BIT1;
	__delay_cycles(80); //approx 1 us delay
	P1OUT &= ~BIT1;   //turn off the trigger
	//TAR = 0;
	while(ECHOPIN == 0);
	TA0R = 0;
	while(ECHOPIN != 0);
	time = TA0R;
	if(time < 800){
		P1OUT |= BIT6;
	}

	TA0CCR0 = 20000;
	TA0R = 0;
	P1SEL |= BIT2;
	__delay_cycles(200000);
	P1OUT &= ~BIT0;
	P1OUT &= ~BIT6;
	return time;
}
