import os
import string
# Daniel Tunitis
# Capt Falkinburg
# The purpose of this program is to find the xor key
#Documentation: c2c Johnson suggested using the algorithm used in this project as a faster method for brute forcing the key
def main():
    #the message to decrypt
    message = [53,223,0,202,93,158,61,219,18,202,93,158,50,200,22,204,18,217,22,144,83,248,1,215,22,208,23,210,10,144,83,249,28,209,23,144,83,249,28,209,23,158]
    i = 0 #Make this 1 to check the other byte of the key
    let = 1
    correct = 2
    res = 0
    for c in range(1, 250):
        #print("A")
        let = c
        while(i < len(message) and correct == 2):
            res = let ^ message[i]
            #print(res)
            if(res != 32 and res != 46 and (res<65 or res>90) and (res<97 or res>122)):
            #This checks to see if the decryption with the current key is not a space, period or capital/lowercase letter
                correct = 0

            else:

                i = i + 2
        if(correct == 2):
            print(hex(c))
            #print("")
        correct = 2
        i = 0

if __name__ == "__main__":
    main()