# Lab2 - Subroutines - "Cryptography"

## By Daniel Tunitis

### Purpose
To make a program that decrypts XOR messages and uses call-by-value and call-by-reference techniques to pass arguments to subroutines.

### Preliminary Design

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/474bb6ca314416295df93a42f23372e947736575/Lab2_Tunitis/flowchart.png?token=3814358f68df7c20b5fbc0e53b0fb0415ef356d2)


Figure 1. Flowchart showing basic design of program

The program idea was to have a main function which would call the decrypt routines. The decrypt message will work by calling decrypt char for each char, storing the result in RAM and then incrementing the RAM and ROM. The Decrypt Char ANDs the key with the byte and stores in R6, then ORs the byte with the key, and stores it in R7. The bits are then cleared in R7 with R6 as source. This is then returned, and the decrypt message continues until the message is completed. 

For B functionality I planned to use the same concept to keep track of message length, but with a reset to beginning when the end of the key was reached. 

To complete the programming I ended up using r5 to hold the key address, r11 for the message, r7 for the size of message, r8 for the RAM address, and r4 for the key size. These would then be passed to the functions, which would in turn use other registers to complete calculations. The final result would be stored in r12


#### Code:

See main.asm for complete code

###### Decrypt Message Subroutine

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/4f97aca0b22e26e66e78cef4a8308fa889804c70/Lab2_Tunitis/decryptmessage1.png?token=5379bde7d17496c88cf7db6d2fb32bf76bbe5e52)

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/4f97aca0b22e26e66e78cef4a8308fa889804c70/Lab2_Tunitis/decryptmessage2.png?token=2b2260f2ce3db69abf0da50358599a7e4f86a5d3)

###### Decrypt Char Subroutine

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/4f97aca0b22e26e66e78cef4a8308fa889804c70/Lab2_Tunitis/decryptchar.png?token=791634518dfce284ab8f8411262cf9b5b4251021)

###### Python Script for A Functionality

 ![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/ad221baf3708a11e273aa9b0546da8184af901e4/Lab2_Tunitis/script.png?token=504038e842a96dee74d0637d0b3d327498de1c96)

This script works by brute forcing the key. It checks an xor value between 0 and 200 for every other byte to see if a character is valid in the key. You can change the value of i to make it check the other character of the xor key. When you run the key for 0 and 1, you get only one output for each, so you know you have the only possible key. 

### Debugging

Some problems were encountered when trying to pass by reference. To figure this out I tested the value changes due to the syntax I used to figure out the proper way in which to perform the pass. 

### Testing Methodology and Results

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/00c24a5249094143c320f64579aae79a21ca2d3f/Lab2_Tunitis/bfunction.png?token=83921eab14d8ca2ff988d024ea317d3e675dfd22)

Above is proof of the B functionality working. To test this I ran the message from the lab document and inputted its corresponding key length. The function decrypted the function into memory as shown. This demonstrates that the function works for varying key lengths and message lengths.

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/ea36e81c67f9591f22a45eab3b7cf0cdb7da9a2e/Lab2_Tunitis/scriptout0.png?token=36d8c3917690dc3b9e822f3eb843daa6fdbaeffc)

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/ea36e81c67f9591f22a45eab3b7cf0cdb7da9a2e/Lab2_Tunitis/scriptout1.png?token=bd1bcbfa1504e5d20a302ef4255fe4efe958d22a)

Above is the output of my python script. The script only decodes one character of the key per run. Thus it has to be run twice for the A message. The key found here was then placed into the assembly program to decode the message.

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/00c24a5249094143c320f64579aae79a21ca2d3f/Lab2_Tunitis/afunction.png?token=b4a1333f0f34ecb87dac6d6a00f11e246cc3071c)

Shown above here is the output of the key found by the python script when used in the assembly program. This further shows proof that the function works for variable key and message lengths. 


### Lab Questions

N/A

### Observations and Conclusions

The purpose of the lab was to decrypt xor messages using subroutines that used pass-by-refrence and call-by-reference techniques. I was successful in the first two functionalities. With A functionality, I did not achieve the entire process in assembly, and used Python for sake of ease. I found this to be a good example of how to easily use push and pops. This concept made more sense after completing this lab. This also showed how it could be easy in the future to need this to free up space in registers for different functions, as registers were limited towards the end. 

### Documentation:
EI from Captain Falkinburg on the program, C2C Johnson suggested the algorithm used for my python script during class