;-------------------------------------------------------------------------------
;Lab 2 Subroutines- "Cryptography"
; C2C Daniel Tunitis, USAF
;
; Purpose: This program uses call-byt-value and call-by-reference techniques
; to decrypt xor-messages using variable length keys and messages
;
; Documentation: EI with Captain Falkinburg
;
;-------------------------------------------------------------------------------
            .cdecls C,LIST,"msp430.h"       ; Include device header file

;-------------------------------------------------------------------------------
            .def    RESET                   ; Export program entry-point to
                                            ; make it known to linker.
;-------------------------------------------------------------------------------
            .text                           ; Assemble into program memory
message: 	.byte	0xf8,0xb7,0x46,0x8c,0xb2,0x46,0xdf,0xac,0x42,0xcb,0xba,0x03,0xc7,0xba,0x5a,0x8c,0xb3,0x46,0xc2,0xb8,0x57,0xc4,0xff,0x4a,0xdf,0xff,0x12,0x9a,0xff,0x41,0xc5,0xab,0x50,0x82,0xff,0x03,0xe5,0xab,0x03,0xc3,0xb1,0x4f,0xd5,0xff,0x40,0xc3,0xb1,0x57,0xcd,0xb6,0x4d,0xdf,0xff,0x4f,0xc9,0xab,0x57,0xc9,0xad,0x50,0x80,0xff,0x53,0xc9,0xad,0x4a,0xc3,0xbb,0x50,0x80,0xff,0x42,0xc2,0xbb,0x03,0xdf,0xaf,0x42,0xcf,0xba,0x50
key:		.byte	0xac, 0xdf, 0x23
size:		.byte	0x51, 0x00

ram_start:	.byte	0x00, 0x02

key_size:   .byte	0x03, 0x00

            .retain                         ; Override ELF conditional linking
                                            ; and retain current section
            .retainrefs                     ; Additionally retain any sections
                                            ; that have references to current
                                            ; section
;-------------------------------------------------------------------------------
RESET       mov.w   #__STACK_END,SP         ; Initialize stackpointer
StopWDT     mov.w   #WDTPW|WDTHOLD,&WDTCTL  ; Stop watchdog timer

;-------------------------------------------------------------------------------
                                            ; Main loop here
;-------------------------------------------------------------------------------

            ;
            ; load registers with necessary info for decryptMessage here
            ;
            mov  	#key, r5
            mov		#message, r11
            mov		#size, r7
            mov.w	#ram_start, r8
            mov		#key_size, r4

            call    #decryptMessage

forever:    jmp     forever

;-------------------------------------------------------------------------------
                                            ; Subroutines
;-------------------------------------------------------------------------------

;-------------------------------------------------------------------------------
;Subroutine Name: decryptMessage
;Author:
;Function: Decrypts a string of bytes and stores the result in memory.  Accepts
;           the address of the encrypted message, address of the key, and address
;           of the decrypted message (pass-by-reference).  Accepts the length of
;           the message by value.  Uses the decryptCharacter subroutine to decrypt
;           each byte of the message.  Stores theresults to the decrypted message
;           location.
;Inputs:	key, addr of message, addr of RAM, size of message
;Outputs:	decrypted message in memory
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptMessage:

			push 		r11
			push 		r5
			push		r6
			push		r9
			push		r8
			push		r10
			push		r7
			push		r14
			push		r15
			mov			r5, r6
			mov.b		@r6+, r9 ; moves the value of the key to r9
			mov		@r8, r10 ; moves the value of the addr of ram to r10

			mov.b		@r7, r14 ; moves the size to r14
			mov.b		@r4, r15 ; move the key size into r15
			dec			r15

start_decrypt:
			mov.b 	@r11, r12 ; moves a byte of the message to r12

			call	#decryptCharacter

			mov.b	r12, 0(r10)
			inc		r10			;increment ram addr
			inc		r11 ; increment a byte up ROM
			;add comparison

			dec		r14 ; decrease remaining bytes
			cmp		r3, r15 ;see if the key needs reseting
			jeq		reset_key
			jnz     set_key
finish:
			cmp		r3, r14
			jne		start_decrypt
			pop		r15
			pop		r14
			pop		r7
			pop		r10
			pop		r8
			pop		r9
			pop		r6
			pop		r5

			pop 	r11
            ret
reset_key:
			mov	@r4, r15	; reset count
			mov 	r5, r6 ;reset the key addr
			mov.b	@r6+, r9
			dec 	r15
			jmp	finish

set_key:
			mov.b		@r6+, r9
			dec			r15
			jmp finish


;-------------------------------------------------------------------------------
;Subroutine Name: decryptCharacter
;Author:
;Function: Decrypts a byte of data by XORing it with a key byte.  Returns the
;           decrypted byte in the same register the encrypted byte was passed in.
;           Expects both the encrypted data and key to be passed by value.
;Inputs:	value of encrypted char, value of hex
;Outputs:	decrypted char
;Registers destroyed:
;-------------------------------------------------------------------------------

decryptCharacter:
			push		r13
			mov		r12, r13
			and		r9,  r13
			or		r9,  r12
			bic		r13, r12 ;r12 is now the xored key, return that
			pop			r13
            ret





;-------------------------------------------------------------------------------
;           Stack Pointer definition
;-------------------------------------------------------------------------------
            .global __STACK_END
            .sect    .stack

;-------------------------------------------------------------------------------
;           Interrupt Vectors
;-------------------------------------------------------------------------------
            .sect   ".reset"                ; MSP430 RESET Vector
            .short  RESET
