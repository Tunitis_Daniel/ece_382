/*--------------------------------------------------------------------
Name: Daniel Tunitis
Date: 6 October 2016
Course: ECE 382
File: pong.c
Event: Assignment 7 - Pong

Purp: Implements a subset of the pong game

Doc:    none

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include "pong.h"
#define TRUE 0x01
#define FALSE 0x00
//Creates a ball and sets its x,y pos, x,y velocity, and radius
ball_t createBall(int xPos, int yPos, int xVel, int yVel, unsigned char radius){
	ball_t userBall;
	userBall.position.x = xPos;
	userBall.position.y = yPos;
	userBall.radius = radius;
	userBall.velocity.x = xVel;
	userBall.velocity.y = yVel;
	return userBall;
}
char rightCheck(ball_t ballToMove){
	if(ballToMove.position.x + ballToMove.velocity.x +ballToMove.radius > SCREEN_WIDTH) return TRUE;
	else return FALSE;
}

char leftCheck(ball_t ballToMove){
	if(ballToMove.position.x + ballToMove.velocity.x - ballToMove.radius< 0) return TRUE;
	else return FALSE;
}

char ceilingCheck(ball_t ballToMove){
	if(ballToMove.position.y + ballToMove.velocity.y -ballToMove.radius< 0) return TRUE;
	else return FALSE;
}

char floorCheck(ball_t ballToMove){
	if(ballToMove.position.y + ballToMove.velocity.y+ballToMove.radius > SCREEN_HEIGHT) return TRUE;
	else return FALSE;
}
//moves the ball location, accounts for boundaries
ball_t moveBall(ball_t ballToMove){

	//check if its hit a horizaontal wall
	if(rightCheck(ballToMove) || leftCheck(ballToMove)){
		ballToMove.velocity.x = 0 - ballToMove.velocity.x; //reverse velocity
	}

	//check if its gonna hit a vertical wall
	if(floorCheck(ballToMove)|| ceilingCheck(ballToMove)){
		ballToMove.velocity.y = 0 - ballToMove.velocity.y; //reverse velocity
	}
	ballToMove.position.y += ballToMove.velocity.y;
	ballToMove.position.x += ballToMove.velocity.x;
	return ballToMove;
}
