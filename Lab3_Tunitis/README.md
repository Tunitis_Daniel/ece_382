# Lab3 - SPI - "I/O"

## By Daniel Tunitis

### Purpose
To use the MSP430 LCD Booster Pack to illustrate software delays and polling. Through this lab we should learn a great deal about how it interfaces with the MSP430 and the process of writing to the lcd screen. It is also a good step towards learning about creating more complicated assembly files. 

### Preliminary Design/Prelab

#####Delay Subroutine:
![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/delay_routine.png?token=0952476298950f6bcb0fc06f3a1cc12ba839d570)

Figure 1: delay subroutine

Above is my delay subroutine. To calculate this I first found my clock's period on the logic analyzer. This was found to be 92 ns. I then calculated a rough estimate of the cycles it would take to reach this. After finalizing this estimation I found myself off by more than 100 microseconds, so I realized the nonsignicant digits became significant after many iterations. Thus, I had to tweak my outloop counter slightly to reach an acceptable time. My end result had a time of 159.988 ms, as shown in the figure below. 

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/delayres.jpg?token=f6f679a6c37b8bebc5470092f7f5ddb0ecca25fa)

Figure 2: delay results

####Pseudocode

drawBox:

get x and y coords
go through a loop, drawing a line between the y coords and changing the x coord until the box is filled in

movingBox:
if button pressed:
	clear box by drawing a box of the background color over it
	if up:
		decrease y coords by 1, drawbox
	if right:
		increase x coords by 1, drawbox
	if left
		decrease x coords by 1, drawbox
	if down
		increase y coords by 1, drawbox

#####ILI9341 LCD BoosterPack:

S1: 	Pin 5,  P1.3

S2:		Pin 8, 	P2.0

S3:		Pin 9,	P2.1

S4:		Pin 10, P2.2

S5:		Pin 1,	P2.3

MOSI:	Pin 15, P1.7

CS:		Pin 2,	P1.0

DC:		Pin 6,  P1.4

MISO:	Pin 1,	P1.6

Signal:	Hex-----PxDIR-------PxREN------PxOut------PxSel------PxSEL2

S1-----0x08-----0 bic-------1 bis------1 bis-------0 bic------0 bic

MOSI---0x80-----From USC1----1 bis-----1bis--------1 bis------1 bis

CS-----0x01-----1 bis--------0 bic-----0 bic-------0 bic------0 bic

#####Configure the MSP430

SCLK: P1.5/ Pin 6, serial clock

CS:	  P1.0/ Pin 2, Chip Select

MOSI: P1.7/ Pin 15, master out slave in

DC:	  P1.4/ Pin 10, Data/Command

P1.5:	Serial Clock

P1.7:	MOSI

P1.6:	MISO

Line 1: setting the UCSWRST bit in the CTL1 register resets the subsystem into a known state until it is cleared

Line 2: Sets the UCCKPH UCMS UCMST UCSYNC bits, setting the address mode, multimaster, master mode, synchronous, with capture on the first clk edge

Line 3: The UCSSEL_2 setting for the UCB0CTL1 register has been chosen, selecting the SMCLK (sub-main clock) as the bit rate source clock for when the MSP 430 is in master mode. 

Line 4: Sets the zero bit in UCB0BR0, setting the bit rate control to one for no clock scaling

Line 5: Makes bit rate control 0 in UCB0BR1

Line 6: Sets the serial clock miso and mosi in p1sel

Line 7: Sets clock miso and mosi in p1sel2

Line 8: Subsystem no longer reset in a known state

#####Draw A Pixel

setArea: Defines the area your about to write to for the LCD

splitColor: splits color word into color bytes

writeData: send the upper byte to the lcd

writeData: send the lower byt to the lcd	

#####Timing Diagrams:


###Code:

See main.asm for the complete code

###Logic Analyzer

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/a98270ebb93884bbce36e7a6095f39b4aae5bc08/Lab3_Tunitis/packets.png?token=612472f2ca7e34de7bb8718b1018497df5b35d41)

Figure 3. Packet table

Above are the packets sent to the LCD from one button press. The logic analyzer output of these is shown below. The readings were taken on the first edge of the clock due to the phase of our clock. Additionally, the chip is set to send MSB first, so the values can be read from left to right. 

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/7f148e0312848fc27095ec40dedc82c3e564bb56/Lab3_Tunitis/diagram.png?token=01b187cf57048edb723077ffb0ec5de8d8d56a5a)

Figure 4. Flowchart of packets

Above is a flowchart detailing the 11 packets sent due to a button press. This shows the order of the packets in an easy to read form. Essentially, there is a column address sent, followed by the parameters specifying the column area. Following this the page address is sent, along with its parameters. Lastly, the data is transferred from the mcu to frame memory. 

Packet 1:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet1.jpg?token=dec7308688e4594b73c8c654701772a0675e999e)


This is the column address set command. 0x2a is the value for this command. It signifies that the next four packets will be the columns for the area to be written. The following four packets are parameters for this command. 

Packet 2:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet2.jpg?token=c81fb29da2fdf80ebcef34ede25d3b40c0a82900)

This is the x start MSB. This specifies the upper bits of the left end of the area to be written. In this case the upper bits are all zero. 

Packet 3:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet3.jpg?token=5ea33ea4e1fd5917a7b5337eef78e4b7a625d0f4)

This is the LSB of the x start parameter. This specifies the lower bits of the left end of the area to be written. In this case the lower bits add up to 0x05. This gives the x start a value of 0x0005. 

Packet 4:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet4.jpg?token=b47d2695b822e596b947d41b15c68b878e87b4b9)

This is the MSB for the end coordinate for x. This specifies the upper bits of the right end of the area to be written. In this case the upper bits are all zero. 

Packet 5:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet5.jpg?token=c8847eadded5482820d4ae2794c1c5d04fa1ef38)

This is the LSB for the end coordinate for x. This specifies the upper bits of the right end of the area to be written. In this case the lower bits add up to 0x0a. This gives the x start a value of 0x000a. That means that the whole x area is between 0x0005 and 0x000a

Packet 6:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet6.jpg?token=5aa4f5f34bf2ef1f69b940870796853b81016eee)

This is the page address set command. 0x2b is the value for this command. It specifies that the next four packets will contain the info for the pages that will be in the area to be written. The next four packets are parameters for this command. 

Packet 7:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet7.jpg?token=e436ccc1476790d67dcfcd54154da8e71c20fc01)

This is the y start MSB. This specifies the upper bits of the upper end of the area to be written. In this case it is 0x00. 

Packet 8:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/f2f670183b080c296f8ead48bb08bfb5474b2b3d/Lab3_Tunitis/packet8.jpg?token=5232a636783d8ac0aeb7720e187327cfebfc6986)

This is the y start LSB. This specifies the lower bits of the upper end of the area to be written. in this case its 0x25, giving y start a value of 0x0025. 

Packet 9:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet9.jpg?token=e03f427cc95338f2934753faadfc773d264de4a6)

This is the y end MSB. This specifies the upper bits of the lower end of the area to be written. In this case it is 0x00. 

Packet 10:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/874c567e90c7791a2709acda4cbeca8f36677310/Lab3_Tunitis/packet10.jpg?token=7b54911db06c86c17a71e5e089a4538876946282)

This is the y end LSB. This specifies the lower bits of the lower end of the area to be written. In this case it is 0x25, giving the y end a value of 0x0025, which means that the area will only be one pixel in the y direction. 

Packet 11:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/f2f670183b080c296f8ead48bb08bfb5474b2b3d/Lab3_Tunitis/packet11.jpg?token=13b662cfc404b43bf017c26a18ca0d863603dbd3)

This byte value (0x2C) transfers the data from the MCU to the memory frame. The LCD can now have pixels drawn on it.

######Writing Modes

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/1cf87d5fb8e14d01f6f70afcf1f889dcb9f4cb1b/Lab3_Tunitis/andxor.png?token=d944e9575c87afb2ad9632911adb9c44f1412954)

Figure 5. Writing modes results

###Results

##### Required Functionality

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/16afa3773781113692eb69e83b28818516da039f/Lab3_Tunitis/required.jpg?token=6a8e0a3f8353246279da889b01ccea1af8d4a797)

Figure 6. Required Functionality

Above is the required functionality. Shown in the picture is a 10x10 square. This was check by Captain Falkinburg.

#####A Functionality: 
For A functionality please see https://drive.google.com/file/d/0B-J7aG3qwxX3ZE1GM2k3VHYxdEk/view?usp=sharing. It is a video of the moving square that moves in the direction of the button press. The video does not have delays between button presses, so it moves very fast. 

#####Bonus Functionality
![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/16afa3773781113692eb69e83b28818516da039f/Lab3_Tunitis/bonus.jpg?token=b5fdd3295aafd4170cd5c72a1a6d75631f130e71)

Figure 7. Bonus functionality

This is the bonus functionality. Captain Warner checked this. It draws my name (Danny). To do this i used my drawBox command I made for the required functionality, several times. It is however very slow and takes up to a minute to actually update the screen. In the future I would want to optimize this. 

I also got A functionality to work. This was checked by Captain Falkinburg. It works by drawing a background colored square over the current square that is drawn, before updating the coords of the new square. The way I went about doing this is however very inefficient, as I copied the entire draw line method again just to change the color. In the future I would instead make drawline accept a color that is passed to it, which would allow me to just reuse the function given without copying the code again. This would also probably make the bonus functionality faster. 

###Conclusions

Overall, the lab has taught me a great deal about using the MSP430 LCD booster pack. It also shows how laziness can cause problems down the road, as my lazy approach to solving the required functionality made my bonus slow in the end. In the future I would want to pass values to methods rather than rewrite methods for slightly different purposes. I also would want to properly space out my work load next time so I spend less time all at once doing the lab. 

####Documentation:

For the prelab C2C Schnall, Prado showed me how to use the logic analyzer. Capt Warner went over diagrams in class. EI with Capt Falkinburg, and Captain Warner. 