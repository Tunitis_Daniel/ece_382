

# Lab 7 - "Robot Sensing" Prelab

**Name:** Daniel Tunitis

**Section:** M6

**Documentation:** EI with Capt Falkinburg, C2C Johnson pointed out that my answer for 1 on the prelab should be muiltiplied by 2

Part I - Understanding the Ultrasonic Sensor and Servo
------------------------------------------------------



#### Ultrasonic Sensor 
1.  How fast does the signal pulse from the sensors travel?
1/58*2 cm/us = 344.8 m/s
2.  If the distance away from the sensor is 1 in, how long does it take for the
    sensor pulse to return to the sensor?  1 cm?
1 in takes 148 us, 1 cm takes 58 us
3.  What is the range and accuracy of the sensor?
2 to 500 cm, res is .3 cm
4.  What is the minimum recommended delay cycle (in ms) for using the sensor?  How does this compare to the "working frequency"? 50 ms, the working frequency is 40 Hz, and this makes it so no obstacle should give the output pin a 38 ms high level signal

#### Servo
1.  Fill out the following table identifying the pulse lengths needed for each servo position:

| Servo Position | Pulse Length (ms) | Pulse Length (counts) |
|----------------|:-----------------:|:---------------------:|
| Left           |       1           |        1000 Theretical Actual 2400           |
| Middle         |       1.5         |        1500           |
| Right          |        2          |        2000 Theoeretical Actual 600          |
    
<br>

Part II - Using the Ultrasonic Sensor and Servo
-----------------------------------------------

1. Create psuedocode and/or flowchart showing how you will *setup* and *use* the ultrasonic sensor and servo.

Sensor: give power from a pin, and output to a seperate pin
Set up the pin as GPIO with input, intput will be from sensor
receive signal
 If greater than min distance threshold: do nothing

else: issue warning to movement system, halt movement in that direction/slight turn away

Servo: give power from a pin
While the robot is in move mode:

give pulse of 1ms, run sensor
give pulse of 1.5 ms, run sensor
give pulse of 2 ms, run sensor


2. Create a schematic showing how you will setup the ultrasonic sensor and servo.

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/65e61863e20a90e64ab7a1d395c680b0bd3a8518/Lab7_Tunitis/schematic.png?token=3bac8265842fcda40777e01f3440d288ea0ec2db)

3. Include any other information from this lab you think will be useful in creating your program.  Small snippets from datasheets such as the ultrasonic sensor timing may be good for your report.

# Debugging

I ran into a few errors on this lab. One error was with my PWM on the servo. I was in OUTMOD_3 rather than OUTMOD_7. This was causing the modulation to be the reverse of what I wanted, and therefore the servo did not work as expected. Capt Falkinburg assisted in this debugging, helping me to use the oscilloscope to determine what was wrong with my code. 

I also had a non-functional ultrasonic sensor. It was not giving echos back, and through Captain Falkinburg's help we were able to use the voltometer and o-scope to fix the issues with my code and the sensor. 

# Testing Methodology
## Required Functionality

Testing required functionality was fairly easy. I just had to verify that the servo turned as needed, and could register an object in front of it, lighting the appropriate LEDs depending on its location. When testing I had fairly long delays, which I should remove when coding Lab 8. 

Video: https://drive.google.com/file/d/0B-J7aG3qwxX3VjBYTmtjS2Nlblk/view?usp=sharing

##B Functionality

Using the ultrasonic sensor data points were gathered to determine the equation for range based on clock counts. This can be seen in the below graph. For data points see the excel document.



![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/30f525dc8cbd3128e257ea4df9d5d6797e69d0b2/Lab7_Tunitis/graph.png?token=a8a99282afa08fe53dedd80ebab7575fe0fa2d6d)

The line of best fit is y = 149.3x + 54.51. The 54.51 is due to the measuring I used when calculating the ranges. I measured from the front of the robot, which is slightly ahead of the ultrasonic sensor. This causes a half of inch of error from the start. Per increased inch the echo time increases by 149.3 clocks, compared to an estimated 148 clocks. This is less than 1 % error, and therefore is fine for my purposes in Lab 8. 

##Bonus Functionality

I created a library of functions for the servo and ultrasonic sensor. These can be viewed at https://bitbucket.org/Tunitis_Daniel/ece_382/src/a91c611ace76e41a5abb01aa83d407856b3404ba/Lab7_Bonus/?at=master

## Observations and Conclusions

This lab forced me to learn how to use some of the hardware in the lab. This made debugging easier and was a valuable learning experience. Additionally, this lab was good at showing how to interface with PWM signals and new types of hardware. I learned a great deal about debugging with the robot. Additionally, I learned more about different clock modes and setting up pins. 