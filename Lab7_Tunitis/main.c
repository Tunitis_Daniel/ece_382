#include <msp430.h> 
#include "movement.h"
#define   	ECHOPIN (P1IN & BIT4)
/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <28 November 2016>
Course: <ECE 382>
File: <main.c>
Event: <Lab 7>

Purp: To demonstrate servo and sensor required functionality.

Doc:    The button code is from Lab 5, setup is from lab 6. C2C Johnson pointed out that you can set up
Ta0 on P1 pins

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

void main(void)
{
    WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
    BCSCTL1 = CALBC1_8MHZ;
    	DCOCTL = CALDCO_8MHZ;  //8 MHz clock
    	P1DIR |= BIT0; //set up LED1
    	P1DIR |= BIT6; //set up LED2
    	P1OUT &= ~BIT0;
    	P1OUT &= ~BIT6;
        P2DIR |= BIT1;                // TA1CCR1 on P2.1
        P2SEL |= BIT1;                // TA1CCR1 on P2.1
        P2OUT &= ~BIT1;
        P2DIR |= BIT4;				//TA1.2 on P2.4
        P2SEL |= BIT4;
        P2OUT &= ~BIT4; //clear output bit

		P1DIR |= BIT2;
		P1SEL |= BIT2;
		P1OUT &= ~BIT2; //set up P1.2 w/ TA0CRR1 for servo

        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
      //  P1DIR = BIT0;            //use LED to indicate duty cycle has toggled
      //  P1REN = BIT3;
	// P1OUT = BIT3;

        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
        TA1CCR1 = 500;                // set duty cycle to 250/1000 (25%)
        TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
        TA1CCR2 = 500;
        TA1CCTL2 |= OUTMOD_3;

        TA0CTL |= TASSEL_2|MC_1|ID_3; //configure for SMCLK
        TA0CCR0 = 20000; //set signal period to ~1.5 ms
        TA0CCR1 = 1500;
        TA0CCTL1 |= OUTMOD_7;

        TA0R = 0; //clear TAR

        P2DIR |= BIT5;   //set up p2.5 as GPIO
        P2SEL &= ~BIT5;
        P2SEL2 &= ~BIT5;
        P2DIR |= BIT3;                      // Set up P2.3 as GPIO not XIN
        P2SEL &= ~BIT3;                                                      // This action takes
        P2SEL2 &= ~BIT3;
        P1DIR |= BIT5;
        P1SEL &= ~BIT5;
        P1SEL2 &= ~BIT5;
        P1OUT &= ~BIT5;


        P1DIR &= ~BIT4; //set up P1.3 as GPIO for echo output from sensor
        P1SEL &= ~BIT4;
        P1SEL2 &= ~BIT4;
        P1DIR |= BIT1; //set up P1.3 as GPIO for input to trig
        P1SEL &= ~BIT1;
        P1SEL2 &= ~BIT1;
        P1OUT &= ~BIT1; //start it default to off
        P1OUT |= BIT2;
        P1OUT &= ~BIT5; //freeze the wheels
        __delay_cycles(2000);
        volatile int time = 0;
        while (1) {
        	/*P1OUT &= BIT1; //set trig
        	__delay_cycles(10);*/
        	//default should be middle for servo
        	TA0CCR1 = 1500;
        	P1OUT |= BIT2;
        	__delay_cycles(2000000);
        	TA0CCR0 = 680000;
        	P1SEL &= ~BIT2;
        	P1OUT |= BIT1;
        	__delay_cycles(80); //approx 1 us delay
        	P1OUT &= ~BIT1;   //turn off the trigger
        	//TAR = 0;
        	while(ECHOPIN == 0);
        	TA0R = 0;
        	while(ECHOPIN != 0);
        	time = TA0R;
        	if(time < 800){
        		P1OUT |= BIT0;
        		P1OUT |= BIT6;
        	}

        	TA0CCR0 = 20000;
        	TA0R = 0;
        	P1SEL |= BIT2;
        	__delay_cycles(20000000);
        	P1OUT &= ~BIT0;
        	P1OUT &= ~BIT6;
        	/*
        	//turn
        	TA0CCR1 = 600; //right turn
        	__delay_cycles(20000000);
        	TA0CCR0 = 680000;
			P1SEL &= ~BIT2;
			P1OUT |= BIT1;
			__delay_cycles(80); //approx 1 us delay
			P1OUT &= ~BIT1;   //turn off the trigger
			//TAR = 0;
			while(ECHOPIN == 0);
			TA0R = 0;
			while(ECHOPIN != 0);
			time = TA0R;
			if(time < 800){
				P1OUT |= BIT6;
			}

			TA0CCR0 = 20000;
			TA0R = 0;
			P1SEL |= BIT2;
			__delay_cycles(20000000);
			P1OUT &= ~BIT0;
			P1OUT &= ~BIT6;
        	TA0CCR1 = 2400; //left turn
        	__delay_cycles(20000000);
        	TA0CCR0 = 680000;
			P1SEL &= ~BIT2;
			P1OUT |= BIT1;
			__delay_cycles(80); //approx 1 us delay
			P1OUT &= ~BIT1;   //turn off the trigger
			//TAR = 0;
			while(ECHOPIN == 0);
			TA0R = 0;
			while(ECHOPIN != 0);
			time = TA0R;
			if(time < 800){
				P1OUT |= BIT0;
			}

			TA0CCR0 = 20000;
			TA0R = 0;
			P1SEL |= BIT2;
			__delay_cycles(20000000);
			P1OUT &= ~BIT0;
			P1OUT &= ~BIT6;*/

        	//__delay_cycles(1500);
        	//read the echo value
        	//TA0CCR1 = 600; //right turn
        	//__delay_cycles(20000000);
        	//TA0CCR1 = 2400; //left turn
        	//__delay_cycles(20000000);
/*
        	P2OUT |= BIT5;
        	P2OUT |= BIT3;
        	__delay_cycles(6000000); //go forward
        	P1OUT &= ~BIT5;
        	//TA1CCR1 = 0;
        	//TA1CCR2 = 0;
        	__delay_cycles(3000000);

        	P1OUT |= BIT5;
        	P2OUT &= ~BIT5;
        	P2OUT &= ~BIT3; //backwards

        	__delay_cycles(6000000);

        	P1OUT &= ~BIT5;

			__delay_cycles(3000000);
			P1OUT |= BIT5;
			P2OUT |= BIT5;
			P2OUT |= BIT3;
			TA1CCR1 = 0;
			__delay_cycles(3000000); //<45 deg right turn
			TA1CCR2 = 0;
			TA1CCR1 = 500;
			P1OUT &= ~BIT5;
			__delay_cycles(6000000);
			P1OUT |= BIT5;
			__delay_cycles(3000000); //<45 deg left turn
			TA1CCR2 = 500;
			P2OUT &= ~BIT5;
			P1OUT &= ~BIT5;
			__delay_cycles(6000000);
			P1OUT |= BIT5;
			__delay_cycles(6000000); //big turn
			P2OUT |= BIT5;
			P2OUT &= ~BIT3;
			__delay_cycles(6000000);//big turn

*/
}}
