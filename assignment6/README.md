##Assignment 6
###By Daniel Tunitis

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/730c8b6a0026e07d4a03ee1827f4fd3952b43c8d/assignment6/assigment6_results.png?token=d1f7c4ef82803fe539048c530ed7a21214ff28b4)
Above is the result of my program.

By looking at the variables screen I can figure out the location in memory of the variables. result1 is stored at 0x03FB, result2 at 0x03FC, result3 at 0x03FD, val1 at 0x03F8, val2 at 0x3F9, and val3 at 0x03FA. These are stored in memory at these locations as they are labeled as volatile variables. i, one, res, THRESHOLD, and two are all stored in registers. These therefore would be erased after the method is done. I can also check these using the memory browser. 

Documentation: C2C Johnson pointed out that the compiler wont let you declare a counter in for loop declarations.  