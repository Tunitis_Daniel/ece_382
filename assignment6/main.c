#include <msp430.h> 
/*--------------------------------------------------------------------
Name:<Daniel Tunitis>
Date:<10/4/2016>
Course: <ECE382>
File:<main.c>
HW: <Assignment 6>

Purp:A goes through and checks three conditions before performing operations as specified in assignment 6

Doc:    C2C Johnson pointed out that you can declare variables in the for loop declaration

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
	volatile char val1 = 0x40;
	volatile char val2 = 0x35;
	volatile char val3 = 0x42;
	volatile char result1 = 0;
	volatile char result2 = 0;
	volatile char result3 = 0;
	#define THRESHOLD 0x38
	if(val1 > THRESHOLD){
		/*store the 10th fibonacci sequence number*/
		char one = 0x00;
		char two = 0x01;
		char res = 0x00;
		int i = 0;
		for(i = 0; i <= 8; i++){
			res = one + two;
			one = two;
			two = res;
		}
		result1 = res;
	}
	if(val2 > THRESHOLD){
		result2 = 0xAF;
	}
	if(val3 > THRESHOLD){
		val2 -= 0x10;
		result3 = val2;
	}
	return 0;
}
