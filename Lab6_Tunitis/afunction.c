/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <15 November 2016>
Course: <ECE 382>
File: <afunction.c>
Event: <Lab 6>

Purp: To operate the robot with button controls from the remote

Doc:    The button code is from Lab 5

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/

#include <msp430g2553.h>
#include "start5.h"
#include "movement.h"

int8	newIrPacket = FALSE;
int16	packetData[48];
int8	packetIndex = 0;
int32   irPacket=0;
int8    started = 0;

#define B_ONE 0x61A000FF
#define B_TWO 0x61A0807F
#define B_THREE 0x61A040BF
#define B_FOUR 0x61A0C03F

// -----------------------------------------------------------------------
// -----------------------------------------------------------------------
void main(void) {
	// Set up MSP to process IR and buttons
	initMSP430();

	// create a variable to ensure that only the first pulse of a press is recorded
	int second = FALSE;

	while(1)  {
		// If a new bit is read
		if(newIrPacket == TRUE){
			newIrPacket = FALSE;				// clear the flag

			if(irPacket == B_ONE){
				if(second == FALSE){
					// Going Forward
					move_forward();
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
			else if(irPacket == B_TWO){
				if(second == FALSE){
					// go backwards
					move_reverse();
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
			else if(irPacket == B_THREE){
				if(second == FALSE){
					//turn right
					move_right();
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}

			}
			else if(irPacket == B_FOUR){
				if(second == FALSE){
					move_left();
					second = TRUE;				// set pulse checker
				}
				else{
					second = FALSE;				// clear pulse checker
				}
			}
		}
	}
} // end main


// -----------------------------------------------------------------------
// In order to decode IR packets, the MSP430 needs to be configured to
// tell time and generate interrupts on positive going edges.  The
// edge sensitivity is used to detect the first incoming IR packet.
// The P2.6 pin change ISR will then toggle the edge sensitivity of
// the interrupt in order to measure the times of the high and low
// pulses arriving from the IR decoder.
//
// The timer must be enabled so that we can tell how long the pulses
// last.  In some degenerate cases, we will need to generate a interrupt
// when the timer rolls over.  This will indicate the end of a packet
// and will be used to alert main that we have a new packet.
// -----------------------------------------------------------------------
void initMSP430() {

	WDTCTL=WDTPW+WDTHOLD; 					// stop WD

	BCSCTL1 = CALBC1_8MHZ;
	DCOCTL = CALDCO_8MHZ;

	P2DIR |= BIT1;                // TA1CCR1 on P2.1
	P2SEL |= BIT1;                // TA1CCR1 on P2.1
	P2OUT &= ~BIT1;
	P2DIR |= BIT4;				//TA1.2 on P2.4
	P2SEL |= BIT4;
	P2OUT &= ~BIT4; //clear output bit

	TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
	P1DIR = BIT0;            //use LED to indicate duty cycle has toggled
	P1REN = BIT3;
	P1OUT = BIT3;

	TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
	TA1CCR1 = 500;                // set duty cycle to 250/1000 (25%)
	TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
	TA1CCR2 = 500;
	TA1CCTL2 |= OUTMOD_3;

	P2DIR &= ~BIT2;                     	// Set up P2.6 as GPIO not XIN
	P2SEL &= ~BIT2;							// This action takes
	P2SEL2 &= ~BIT2;						// three lines of code.

	P2DIR |= BIT5;   //set up p2.5 as GPIO
	P2SEL &= ~BIT5;
	P2SEL2 &= ~BIT5;
	P2DIR |= BIT3;                      // Set up P2.3 as GPIO not XIN
	P2SEL &= ~BIT3;                                                      // This action takes
	P2SEL2 &= ~BIT3;
	P1DIR |= BIT5;
	P1SEL &= ~BIT5;
	P1SEL2 &= ~BIT5;
	P1OUT &= ~BIT5;

	P2IFG &= ~BIT2;							// Clear any interrupt flag on P2.2
	P2IE  |= BIT2;							// Enable P2.2 interrupt

	HIGH_2_LOW;								// check the header out.  P2IES changed.


	TA0R = 0;
	TACCR0 = 0x3E80;						// 16 ms

	// create a 16ms roll-over period
	TA0CTL &= ~TAIFG;						// clear flag before enabling interrupts = good practice
	TA0CTL = ID_3 | TASSEL_2;				// Use 1:8 prescalar off SMCLK and enable interrupts

	// enable interrupts
	_enable_interrupt();
	P1OUT &= ~BIT5;
	P2OUT &= ~BIT5;
	P2OUT &= ~BIT3;
}

// -----------------------------------------------------------------------
// Since the IR decoder is connected to P2.6, we want an interrupt
// to occur every time that the pin changes - this will occur on
// a positive edge and a negative edge.
//
// Negative Edge:
// The negative edge is associated with end of the logic 1 half-bit and
// the start of the logic 0 half of the bit.  The timer contains the
// duration of the logic 1 pulse, so we'll pull that out, process it
// and store the bit in the global irPacket variable. Going forward there
// is really nothing interesting that happens in this period, because all
// the logic 0 half-bits have the same period.  So we will turn off
// the timer interrupts and wait for the next (positive) edge on P2.6
//
// Positive Edge:
// The positive edge is associated with the end of the logic 0 half-bit
// and the start of the logic 1 half-bit.  There is nothing to do in
// terms of the logic 0 half bit because it does not encode any useful
// information.  On the other hand, we going into the logic 1 half of the bit
// and the portion which determines the bit value, the start of the
// packet, or if the timer rolls over, the end of the ir packet.
// Since the duration of this half-bit determines the outcome
// we will turn on the timer and its associated interrupt.
// -----------------------------------------------------------------------
#pragma vector = PORT2_VECTOR					// This is from the MSP430G2553.h file

__interrupt void pinChange (void) {
	int8	pin;
	int16	pulseDuration;						// The timer is 16-bits

	if (IR_PIN)	pin=1;	else pin=0;

	// read the current pin level
	switch (pin) {
		// !!!!!!!!!NEGATIVE EDGE!!!!!!!!!!
		case 0:
			pulseDuration = TA0R;

			//classify logic 1 half pulse and shift bit into irpacket
			if(pulseDuration < maxLogic1Pulse && pulseDuration > minLogic1Pulse){
				irPacket = irPacket << 1;
				irPacket++;
			}
			else{
				irPacket = irPacket << 1;
			}
			TA0CTL &= ~MC_1;      				// turn off timer a
			LOW_2_HIGH; 						// set up pin interrupt on positive edge
			break;

		// !!!!!!!!POSITIVE EDGE!!!!!!!!!!!
		case 1:
			TA0R = 0x0000;						// time measurements are based at time 0
			TA0CTL |= MC_1;						// turn on timer a
			TACTL |= TAIE;						// turn on interrupt
			HIGH_2_LOW; 						// set up pin interrupt on falling edge
			break;
	} // end switch

	P2IFG &= ~BIT2;								// clear the interrupt flag to prevent immediate ISR re-entry

} // end pinChange ISR

// This is from the MSP430G2553.h file
#pragma vector = TIMER0_A1_VECTOR
__interrupt void timerOverflow (void) {
	TA0CTL &= ~MC_1; 							// turn off timer a
	TACTL &= ~TAIE; 							// turn off interrupt
	newIrPacket = TRUE;							// set newPacket flag
	TA0CTL &= ~TAIFG; 							// clear taifg
}
