/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <15 November 2016>
Course: <ECE 382>
File: <movement.c>
Event: <Lab 6>

Purp: Controls to move robot

Doc:    The button code is from Lab 5

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430g2553.h>
#include "movement.h"

void move_forward(){
	P1OUT |= BIT5;
	P2OUT |= BIT5;
	P2OUT |= BIT3;
	__delay_cycles(6000000); //go forward
	P1OUT &= ~BIT5;
}
void move_reverse(){
	P1OUT |= BIT5;
	P2OUT &= ~BIT5;
	P2OUT &= ~BIT3;
	__delay_cycles(6000000);
	P1OUT &= ~BIT5;
}
void move_left(){
	P1OUT |= BIT5;
	P2OUT |= BIT5;
	P2OUT |= BIT3;
	TA1CCR1 = 0;
	__delay_cycles(3000000); //<45 deg right turn
	P1OUT &= ~BIT5;
	TA1CCR1 = 500;
}
void move_right(){
	P1OUT |= BIT5;
	P2OUT |= BIT5;
	P2OUT |= BIT3;
	TA1CCR2 = 0;
	__delay_cycles(3000000);
	P1OUT &= ~BIT5;
	TA1CCR2 = 500;
}


