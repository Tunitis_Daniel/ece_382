#Lab 6 - PWM - "Robot Motion"

###Purpose:

The main purpose of this lab is learning to interface our MSP430s with the Robots and achieve movement for future labs. Additionally, this lab serves as a link to the past lab, as we implement remote control after completing the required functionality. 

###Prelab:

#####Parts:
1 motor driving chip, 1 decoupling capacitor, 5V to 3.3 V regulator, motor driver chip, breadboard, Robot, ample wires. Important ntoe: For this lab the MSP430 should be put in the breadboard and the launchpad used as a holder.

#####Important information on programming the MSP430 in the circuit:

1.Remove your MSP430 from your Launchpad and place it in the breadboard.

2.Remove the TEST, RST, and VCC jumpers from your Launchpad.

3.Connect wires to the TEST, RST, and VCC pins closest to the microUSB connector.◦Connect TEST to Pin 17 of your MSP430
◦Connect RST to Pin 16 of your MSP430
◦Connect VCC to Pin 1 of your MSP430

4.Connect the GND pin on your MSP430 to ground on your breadboard.

5.Connect the VCC pin on your MSP430 to voltage on your breadboard - not more than 3.3V!

Plan is to use P2.4 and P2.1 for the duty cycles, P2.5, P2.6 for GPIO for reverse/forward on the wheels, and P1.5 for the general purpose on/off enable GPIO. 

The duty cycle and GPIO will be alternated on the engine inputs, as this will allow one setting to go forward for both and backward for both. To acheive backwards movement I just need to toggle the GPIO on 2.5 and 2.6. To move left or right I need to toggle one corresponding to the side I wish to turn on. 

For PWM I will use P2.1 and P2.4, and thus BIT1 and Bit4 need to be set for P2DIR and P2SEL, while P2OUT needs to be cleared at these bits. 

For movement methods I will make a move forward, left, right, and backwards method.

For Schematic Design please see the fritz file. Below is the implementation of this design:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/5899a418f8144ea5b74c29af763dfaeb2d103c48/Lab6_Tunitis/board.jpg?token=d8cd5840f40808a7df759f201e49d8956637d00e)

Schematic:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/f0b59742f970bd2f457dfb26ddb454c610bf9028/Lab6_Tunitis/fritz.png?token=e25818681f00a7f9417470b3217a434562e415a4)

Flowchart:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/f0b59742f970bd2f457dfb26ddb454c610bf9028/Lab6_Tunitis/flowgraph.png?token=582249833dcd32e8b276e6865747b8879c453a86)

Movement Functions:

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/2127a07932c11631a8080ba41d10155a121a19f3/Lab6_Tunitis/functions.png?token=353cf3b949d6c87e175661ee7277d5786c9f6cf3)

For A functionality I planned to take my Lab 5 code and replace the LED setting code with functions that move the robot in the desired direction. These functions are shown in the above flowchart. 

### Testing Methodology

To test required functionality I set up an infinite loop demonstrating forward, backwards, left slight turn, right slight turn, big right turn, and big left turn. This would run indefinitely demonstrating the needed functionality for the robot. This was checked by Captain Falkinburg. See requiredlab6.mp4 for the video of functionality.

To test the A functionality I used my remote and verified the robot moved as commanded by the remote. I also verified that a continuous hold would allow it to continue to move forward/backwards/turn. This was checked by Captain Warner. Functionality is also available in labfunctiona.mp4.

### Debugging

My biggest errors were in wiring. I did not properly ground certain instances, which caused my motors not to work. I also did not initially ground my heat sink, but this did not cause issues. I did however fix that issue to prevent damage to my motor driver chip. Additionally in testing I found some wires came lose and I had to fix those. 

In coding I was setting the GPIOs as input rather than output, and Capt Falkinburg pointed this out, helping me to complete the functionality. 

### Observations and Conclusions

This lab required me to learn a great deal about wiring. The wiring of the robot took more time than programming it for me. I also learned a good deal about the motor driver chip, as I relied heavily upon the datasheet for it to complete my wiring properly. This lab however was a good lab for learning how to interface with other parts to produce cool results.

#### Documentation:

EI with Capt Falkinburg/Warner, reused lab5 code 



