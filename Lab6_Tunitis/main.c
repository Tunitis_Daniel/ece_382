/*--------------------------------------------------------------------
Name: <Daniel Tunitis>
Date: <15 November 2016>
Course: <ECE 382>
File: <main.c>
Event: <Lab 6>

Purp: To demonstrate robot going forward, backward, slight turn left and right, big turn left and right

Doc:    The button code is from Lab 5

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
#include <msp430.h>

void main(void)
{
    WDTCTL = WDTPW|WDTHOLD;                 // stop the watchdog timer
    BCSCTL1 = CALBC1_8MHZ;
    	DCOCTL = CALDCO_8MHZ;  //8 MHz clock

        P2DIR |= BIT1;                // TA1CCR1 on P2.1
        P2SEL |= BIT1;                // TA1CCR1 on P2.1
        P2OUT &= ~BIT1;
        P2DIR |= BIT4;				//TA1.2 on P2.4
        P2SEL |= BIT4;
        P2OUT &= ~BIT4; //clear output bit

        TA1CTL |= TASSEL_2|MC_1|ID_0;           // configure for SMCLK
        P1DIR = BIT0;            //use LED to indicate duty cycle has toggled
        P1REN = BIT3;
        P1OUT = BIT3;

        TA1CCR0 = 1000;                // set signal period to 1000 clock cycles (~1 millisecond)
        TA1CCR1 = 500;                // set duty cycle to 250/1000 (25%)
        TA1CCTL1 |= OUTMOD_3;        // set TACCTL1 to Set / Reset mode
        TA1CCR2 = 500;
        TA1CCTL2 |= OUTMOD_3;



        P2DIR |= BIT5;   //set up p2.5 as GPIO
        P2SEL &= ~BIT5;
        P2SEL2 &= ~BIT5;
        P2DIR |= BIT3;                      // Set up P2.3 as GPIO not XIN
        P2SEL &= ~BIT3;                                                      // This action takes
        P2SEL2 &= ~BIT3;
        P1DIR |= BIT5;
        P1SEL &= ~BIT5;
        P1SEL2 &= ~BIT5;
        P1OUT &= ~BIT5;

        while(P1IN & BIT3);
        P1OUT |= BIT5;
        while (1) {


        	P2OUT |= BIT5;
        	P2OUT |= BIT3;
        	__delay_cycles(6000000); //go forward
        	P1OUT &= ~BIT5;
        	//TA1CCR1 = 0;
        	//TA1CCR2 = 0;
        	__delay_cycles(3000000);

        	P1OUT |= BIT5;
        	P2OUT &= ~BIT5;
        	P2OUT &= ~BIT3; //backwards

        	__delay_cycles(6000000);

        	P1OUT &= ~BIT5;

			__delay_cycles(3000000);
			P1OUT |= BIT5;
			P2OUT |= BIT5;
			P2OUT |= BIT3;
			TA1CCR1 = 0;
			__delay_cycles(3000000); //<45 deg right turn
			TA1CCR2 = 0;
			TA1CCR1 = 500;
			P1OUT &= ~BIT5;
			__delay_cycles(6000000);
			P1OUT |= BIT5;
			__delay_cycles(3000000); //<45 deg left turn
			TA1CCR2 = 500;
			P2OUT &= ~BIT5;
			P1OUT &= ~BIT5;
			__delay_cycles(6000000);
			P1OUT |= BIT5;
			__delay_cycles(6000000); //big turn
			P2OUT |= BIT5;
			P2OUT &= ~BIT3;
			__delay_cycles(6000000);//big turn


}}
