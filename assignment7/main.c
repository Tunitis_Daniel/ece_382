#include <msp430.h> 
#include "pong.h"
/*--------------------------------------------------------------------
Name: Daniel Tunitis
Date: 6 October 2016
Course: ECE 382
File: main.c
Event: Assignment 7 - Pong

Purp: run the pong game

Doc:    None

Academic Integrity Statement: I certify that, while others may have
assisted me in brain storming, debugging and validating this program,
the program itself is my own work. I understand that submitting code
which is the work of other individuals is a violation of the honor
code.  I also understand that if I knowingly give my original work to
another individual is also a violation of the honor code.
-------------------------------------------------------------------------*/
int main(void) {
    WDTCTL = WDTPW | WDTHOLD;	// Stop watchdog timer
    ball_t ball = createBall(481, 471, 10, 10, 0x000a);
    ball = moveBall(ball); //ball should now reverse direction and start heading left
    ball = moveBall(ball); //ball should move normally to the left and up to ceiling
    ball = moveBall(ball); //ball should travel normally
    //test ceiling and left side *****at this point x should be 451, y should be 461, -10 velocity both directions
    ball.position.y = 19;
    ball.position.x = 29;
    ball = moveBall(ball); //bounce right
    ball = moveBall(ball); //bounce down
    //at this point y should be 39, x should be 29
    //ball is now going positive in both
    //verify corner works
    ball.position.x = 481;
    ball.position.y = 481;
    ball = moveBall(ball);
    ball = moveBall(ball);
    // at this point x and y should be 461, both should have negative -10 velocity
	return 0;
}
