#Assignment 7 - "Pong"
### Daniel Tunitis

◦How did you verify your code functions correctly?

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/00e36309837b20e25ed22528f1730c2db4ccace7/assignment7/bp1.png?token=44005482013e8a2533adc269f8b5f459ffb9c1df)

######Figure 1: Test hitting right wall and floor

This verifies that the ball will bounce off the right and change directions to head left. It also verifies that it will bounce off the bottom of the screen (the max bound for y). 

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/00e36309837b20e25ed22528f1730c2db4ccace7/assignment7/bp2.png?token=8ed434a510b768ebcee9f1246c19e763f6e72252)

######Figure 2: Test hitting the left wall and ceiling

This verifies that the ball will bounce off the left and change directions to head right. It also verifies that it will bounce off the top of the screen (the min bound for y).

![](https://bytebucket.org/Tunitis_Daniel/ece_382/raw/00e36309837b20e25ed22528f1730c2db4ccace7/assignment7/bp3.png?token=b8057e418d1869f7687c9016115ce9af9d486644)

######Figure 3: Test a corner

This verifies that the ball will change direction in the x and y coordinates if it hits a corner.

Ultimately, these test cases show that the ball will stay inside the bounds of the screen at all times, provided it starts in the screen.

◦How could you make the "collision detection" helper functions only visible to your implementation file (i.e. your main.c could not call those functions directly)?

I dont have these functions in my pong.h file, so they are not included with the include pong.h call in my main.c. 

####Documentation: None